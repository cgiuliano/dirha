log4j.rootLogger=debug,stdout
#log4j.rootLogger=info,stdout

log4j.appender.stdout=org.apache.log4j.ConsoleAppender
log4j.appender.stdout.Encoding=UTF-8
log4j.appender.stdout.layout=org.apache.log4j.PatternLayout

# Pattern to output the caller's file name and line number.
#log4j.appender.stdout.layout.ConversionPattern=[%t] %-5p %c %x (%F:%L) - %m %n
log4j.appender.stdout.layout.ConversionPattern=[%t] %-5p (%F:%L) - %m %n
#log4j.appender.stdout.layout.ConversionPattern=[%t] %-5p %c %x - %m%n
#log4j.appender.stdout.layout.ConversionPattern=[%t] %-5p %m %n

#log4j.appender.R=org.apache.log4j.RollingFileAppender
#log4j.appender.R.File=jwikify.log
#log4j.appender.R.Encoding=UTF-8
#log4j.appender.R.MaxFileSize=1000KB
# Keep one backup file
#log4j.appender.R.MaxBackupIndex=1

#log4j.appender.R.layout=org.apache.log4j.PatternLayout
#log4j.appender.R.layout.ConversionPattern=%d %-4r [%t] %-5p %c %x - %m%n
#log4j.appender.R.layout.ConversionPattern=%d %-4r [%t] %-5p - %m%n

# Print only messages of level WARN or above in the package org.apache.commons
log4j.logger.org.apache.commons=WARN

# Print only messages of level WARN or above in the package org.apache.axis
log4j.logger.org.apache.axis=WARN

# Print only messages of level WARN or above in the package web1t
#log4j.logger.org.fbk.irst.tcc.web1t=WARN

# Print only messages of level WARN or above in the package mylibsvm
log4j.logger.org.fbk.cit.hlt.core.mylibsvm=WARN

# Print only messagees of level WARN or above in the package langidentifier
#log4j.logger.org.fbk.cit.hlt.langidentifier=WARN
